FROM containers.ligo.org/lscsoft/gstlal:nonstd_gwaves_2021

USER root

# install nonstd-gwaves
COPY nonstd-gwaves /nonstd-gwaves
RUN cd /nonstd-gwaves && \
    python3 setup.py install --root=/ --single-version-externally-managed
RUN rm -rf /nonstd-gwaves

ENTRYPOINT []
CMD bash
